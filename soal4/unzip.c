#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void* DownloadFile(void* arg) {
    const char* url = (const char*)arg;
    char command[256];

    sprintf(command, "wget --no-check-certificate '%s' -O hehe.zip", url);

    system(command);

    pthread_exit(NULL);
}

int main() {
    pthread_t thread;
    const char* url = "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp";
    const char* zipFilename = "hehe.zip";

    if (pthread_create(&thread, NULL, DownloadFile, (void*)url) != 0) {
        fprintf(stderr, "Failed to create thread.\n");
        return 1;
    }

    pthread_join(thread, NULL);

    system("unzip hehe.zip -d hehe");

    return 0;
}
