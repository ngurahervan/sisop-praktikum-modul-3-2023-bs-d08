#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <ctype.h>
#include <time.h>

typedef struct{
    char direktori[1000];
    int total;
} direk;

direk allFolder[1000];
direk extension[100];

int cmpfunc(const void *a, const void *b) {
    const direk *rec1 = (direk *)a;
    const direk *rec2 = (direk *)b;
    return strcmp(rec1->direktori,rec2->direktori);
}
int main(int argc, char const *argv[])
{
    FILE *f;
    f = fopen("hehe/log.txt","r");

    if (f == NULL) {
        printf("Failed to open file\n");
        return 1;
    }

    char s[1000];
    int count = 0;
    int idx = 0;
    int idx2 = 0;

    while (fgets(s, sizeof(s), f)) {
        s[strcspn(s, "\n")] = '\0';
        if(strstr(s, "ACCESSED") != NULL){
            count++;
        }
        
        if(strstr(s, "MADE") != NULL){
            char *k = strrchr(s, '/');
            if (k != NULL) {
                allFolder[idx].total = 0;
                strcpy(allFolder[idx++].direktori, k+1);
                char *l= strchr(k,'(');
                if (l == NULL)
                {
                    extension[idx2].total = 0;
                    strcpy(extension[idx2++].direktori, k+1);
                }
                
            }
        }
        if(strstr(s, "MOVED") != NULL){
            char *temp = strrchr(s, '/');
            if (temp != NULL) {
                for (int i = 0; i < idx; i++) {
                    if (strcmp(allFolder[i].direktori, temp+1) == 0) {
                        allFolder[i].total++;
                    }
                }
            }
        }
    }

    fclose(f);
    printf("ACCESSED : %d",count);
    
    qsort(allFolder,idx,sizeof(direk),cmpfunc);

    for (int i = 0; i < idx; i++) {
        printf("%s : %d\n", allFolder[i].direktori, allFolder[i].total);
    }
    for (int i = 0; i < idx2; i++){
        for (int j = 0; j < idx; j++){
            if (strstr(allFolder[j].direktori,extension[i].direktori)!=NULL){
                extension[i].total+=allFolder[j].total;
            }
        }
    }
    printf("Folder Ekstensi\n");
    qsort(extension,idx2,sizeof(direk),cmpfunc);
    for (int i = 0; i < idx2; i++) {
        printf("%s : %d\n", extension[i].direktori, extension[i].total);
    }
    return 0;
}
