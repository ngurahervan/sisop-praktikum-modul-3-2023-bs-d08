#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>

#define MAX_PATH_LENGTH 256
#define MAX_EXTENSIONS 100
#define MAX_THREADS 10

typedef struct {
    char srcPath[MAX_PATH_LENGTH];
    char dstPath[MAX_PATH_LENGTH];
} MoveThreadArgs;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

char extensions[MAX_EXTENSIONS][10];
int numExtensions = 0;
int maxCounts[MAX_EXTENSIONS] = {0};
int otherCount = 0;

void lowercase(char* str) {
    while (*str) {
        *str = tolower((unsigned char)*str);
        str++;
    }
}

void getTime(char* timeStr) {
    time_t rawtime;
    struct tm* timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(timeStr, 20, "%d-%m-%Y %H:%M:%S", timeinfo);
}

void ensureFolderExists(const char* folderPath) {
    struct stat st;
    if (stat(folderPath, &st) == -1) {
        // Folder belum ada, buat folder baru
        if (mkdir(folderPath, 0777) != 0) {
            printf("Error creating folder: %s\n", folderPath);
            exit(1);
        }
    }
}

void moveFileToCategory(const char* fileName, const char* category) {
    char sourcePath[MAX_PATH_LENGTH];
    char destPath[MAX_PATH_LENGTH];
    sprintf(sourcePath, "hehe/files/%s", fileName);
    sprintf(destPath, "hehe/files/categorized/%s/%s", category, fileName);

    // Memindahkan file
    if (rename(sourcePath, destPath) != 0) {
        printf("Error moving file: %s\n", fileName);
        exit(1);
    }
}

void getFolderPath(const char* filePath, const char* rootFolder, char* folderPath) {
    char* relativePath = strstr(filePath, rootFolder);
    if (relativePath != NULL) {
        strncpy(folderPath, filePath, relativePath - filePath + strlen(rootFolder));
        folderPath[relativePath - filePath + strlen(rootFolder)] = '\0';
    } else {
        strcpy(folderPath, "");
    }
}

void ensureLogFileExists() {
    FILE* logFile = fopen("hehe/log.txt", "a");
    if (!logFile) {
        // File log.txt belum ada, buat file baru
        logFile = fopen("hehe/log.txt", "w");
        if (!logFile) {
            printf("Error creating log file\n");
            return;
        }
        fclose(logFile);
    }
}

void ensureCategorizedFolderExists() {
    char folderPath[MAX_PATH_LENGTH];
    snprintf(folderPath, MAX_PATH_LENGTH, "hehe/files/categorized");

    ensureFolderExists(folderPath);
}

void ensureFoldersExist() {
    ensureFolderExists("hehe");
    ensureFolderExists("hehe/files");
    ensureCategorizedFolderExists();
}

void logAccessed(const char* folderPath) {
	ensureLogFileExists();
    char timeStr[20];
    getTime(timeStr);
    FILE* logFile = fopen("hehe/log.txt", "a");
    if (logFile) {
    fprintf(logFile,"%s ACCESSED [%s]\n", timeStr, folderPath);
    fclose(logFile);
}
}

void logMoved(const char* extension, const char* srcPath, const char* dstPath) {
    ensureLogFileExists();
	char timeStr[20];
    getTime(timeStr);
    FILE* logFile = fopen("hehe/log.txt", "a");
    if (logFile) {
        fprintf(logFile, "%s MOVED [%s] file: [%s] > [%s]\n", timeStr, extension, srcPath, dstPath);
        fclose(logFile);
    }
}

void logMade(const char* folderName) {
    ensureLogFileExists();
	char timeStr[20];
    getTime(timeStr);
    FILE* logFile = fopen("hehe/log.txt", "a");
    if (logFile) {
        fprintf(logFile, "%s MADE [%s]\n", timeStr, folderName);
        fclose(logFile);
    }
}

void* moveFile(void* arg) {
    MoveThreadArgs* threadArgs = (MoveThreadArgs*)arg;
    char srcPath[MAX_PATH_LENGTH];
    char dstPath[MAX_PATH_LENGTH];

    strcpy(srcPath, threadArgs->srcPath);
    strcpy(dstPath, threadArgs->dstPath);

    free(arg);

    pthread_mutex_lock(&mutex);
    logMoved(srcPath, srcPath, dstPath);
    pthread_mutex_unlock(&mutex);

    if (rename(srcPath, dstPath) != 0) {
        pthread_mutex_lock(&mutex);
        printf("Error moving file: %s\n", srcPath);
        pthread_mutex_unlock(&mutex);
    }

    pthread_cond_signal(&cond);
    return NULL;
}

void handleFile(const char* filePath, const char* rootFolder) {
    char extension[10];
    char dstFolder[MAX_PATH_LENGTH];
    char dstPath[MAX_PATH_LENGTH];

    int i;
    for (i = strlen(filePath) - 1; i >= 0; i--) {
        if (filePath[i] == '.') {
            strcpy(extension, &filePath[i + 1]);
            lowercase(extension);
            break;
        }
    }

if (i < 0) {
    strcpy(extension, "other");
}

pthread_mutex_lock(&mutex);

for (i = 0; i < numExtensions; i++) {
    if (strcmp(extension, extensions[i]) == 0) {
        break;
    }
}

if (i == numExtensions) {
        if (numExtensions >= MAX_EXTENSIONS) {
            pthread_mutex_unlock(&mutex);
            pthread_mutex_lock(&mutex);
            printf("Maximum number of extensions exceeded\n");
            pthread_mutex_unlock(&mutex);
            return;
        }

        if (strcmp(extension, "other") == 0) {
            pthread_mutex_lock(&mutex);
            otherCount++;
            pthread_mutex_unlock(&mutex);
        } else {
            strcpy(extensions[numExtensions], extension);
            numExtensions++;
            logMade(extension);
        }
    }
    
int count = maxCounts[i]++;
pthread_mutex_unlock(&mutex);

char folderPath[MAX_PATH_LENGTH];
getFolderPath(filePath, rootFolder, folderPath);

if (i == numExtensions - 1 && count >= maxCounts[i]) {
    snprintf(dstFolder, MAX_PATH_LENGTH + 44, "hehe/files/%s/%s (%d)", folderPath, extension, count + 2);
} else {
    snprintf(dstFolder, MAX_PATH_LENGTH + 44, "hehe/files/%s/%s", folderPath, extension);
}
snprintf(dstPath, MAX_PATH_LENGTH + 44, "hehe/files/%s/%s", dstFolder, filePath);

pthread_mutex_lock(&mutex);

DIR* dir = opendir("hehe/files");
if (!dir) {
    if (mkdir(dstFolder, 0777) != 0) {
        pthread_mutex_unlock(&mutex);
        pthread_mutex_lock(&mutex);
        printf("Error creating folder: %s\n", dstFolder);
        pthread_mutex_unlock(&mutex);
        return;
    }
    logMade(dstFolder);
} else {
    closedir(dir);
}

pthread_mutex_unlock(&mutex);

MoveThreadArgs* threadArgs = (MoveThreadArgs*)malloc(sizeof(MoveThreadArgs));
snprintf(threadArgs->srcPath, MAX_PATH_LENGTH, "hehe/files/categorized/%s", filePath);
snprintf(threadArgs->dstPath, MAX_PATH_LENGTH + 44, "hehe/files/categorized/%s", dstPath);

pthread_mutex_lock(&mutex);
logMoved(extension, filePath, dstPath);
pthread_mutex_unlock(&mutex);

pthread_t tid;
pthread_create(&tid, NULL, moveFile, threadArgs);

pthread_mutex_lock(&mutex);
while (pthread_cond_wait(&cond, &mutex) != 0);
pthread_mutex_unlock(&mutex);
}

void* processFiles(void* arg) {
char folderPath[MAX_PATH_LENGTH];
strcpy(folderPath, (char*)arg);
DIR* dir = opendir(folderPath);
if (!dir) {
printf("Error opening folder: %s\n", folderPath);
return NULL;
}
struct dirent* entry;
while ((entry = readdir(dir)) != NULL) {
    if (entry->d_type == DT_REG) {
        handleFile(entry->d_name, folderPath);
    } else if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
        char subFolderPath[MAX_PATH_LENGTH];
        snprintf(subFolderPath, MAX_PATH_LENGTH + 256, "%s/%s", folderPath, entry->d_name);
        logAccessed(subFolderPath);

        pthread_t tid;
        pthread_create(&tid, NULL, processFiles, strdup(subFolderPath));
    }
}

closedir(dir);
free(arg);
return NULL;
}

void printFileCounts() {
    printf("File counts by extension (ascending order):\n");

    // Buat salinan array maxCounts dan extensions
    int counts[MAX_EXTENSIONS];
    char extensionsCopy[MAX_EXTENSIONS][10];
    memcpy(counts, maxCounts, sizeof(maxCounts));
    memcpy(extensionsCopy, extensions, sizeof(extensions));

    // Bubble sort untuk mengurutkan counts dan extensionsCopy
    for (int i = 0; i < numExtensions - 1; i++) {
        for (int j = 0; j < numExtensions - i - 1; j++) {
            if (strcmp(extensionsCopy[j], extensionsCopy[j + 1]) > 0) {
                int tempCount = counts[j];
                counts[j] = counts[j + 1];
                counts[j + 1] = tempCount;

                char tempExtension[10];
                strcpy(tempExtension, extensionsCopy[j]);
                strcpy(extensionsCopy[j], extensionsCopy[j + 1]);
                strcpy(extensionsCopy[j + 1], tempExtension);
            }
        }
    }

    // Cetak counts dan extensionsCopy
    for (int i = 0; i < numExtensions; i++) {
        printf("%s : %d\n", extensionsCopy[i], counts[i]);
    }

    printf("other : %d\n", otherCount);
    printf("\n");
}

int main() {
FILE* extFile = fopen("hehe/extensions.txt", "r");
if (!extFile) {
printf("Error opening extensions.txt\n");
return 1;
}

int main(); {
	ensureFoldersExist();
}

char extension[10];
while (fscanf(extFile, "%s", extension) == 1) {
    lowercase(extension);
    strcpy(extensions[numExtensions], extension);
    numExtensions++;
}

fclose(extFile);
FILE* maxFile = fopen("hehe/max.txt", "r");
if (!maxFile) {
    printf("Error opening max.txt\n");
    return 1;
}

int count;
int i = 0;
while (fscanf(maxFile, "%d", &count) == 1) {
    maxCounts[i] = count;
    i++;
}

fclose(maxFile);

char rootFolder[MAX_PATH_LENGTH] = "hehe/files";
logAccessed(rootFolder);

pthread_t tid;
pthread_create(&tid, NULL, processFiles, strdup(rootFolder));

pthread_join(tid, NULL);
printFileCounts();
printf("\nFile counts by extension:\n");

pthread_mutex_lock(&mutex);
for (int i = 0; i < numExtensions; i++) {
    printf("%s : %d\n", extensions[i], maxCounts[i]);
}
printf("other : %d\n", otherCount);
pthread_mutex_unlock(&mutex);
pthread_mutex_destroy(&mutex);
pthread_cond_destroy(&cond);

return 0;
}
