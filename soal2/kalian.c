#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

int R1 = 4; // Baris 1
int C1 = 2; // Kolom 1
int R2 = 2; // Baris 2
int C2 = 5; // Kolom 2
int min1 = 1; // Batas
int max1 = 5;
int min2 = 1;
int max2 = 4;

// fungsi untuk membuat matrix 
void create_matrix(int rows, int cols, int matrix[rows][cols], int range_min, int range_max) {

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            matrix[i][j] = range_min + (i*cols + j) % (range_max - range_min + 1);
        }
    }

    for (int i = rows*cols - 1; i > 0; i--) {
        int j = rand() % (i + 1);
        int temp = matrix[i / cols][i % cols];
        matrix[i / cols][i % cols] = matrix[j / cols][j % cols];
        matrix[j / cols][j % cols] = temp;
    }
}

// fungsi untuk menampilkan matrix yang telah di create 
void display_matrix(int rows, int cols, int matrix[rows][cols]) {
    int i, j;
    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n"); // menambahkan line ketika column sudah habis
    }
}

void multiply(int R1, int C1, int R2, int C2, int mat1[R1][C1], int mat2[R2][C2], int result[R1][C2]) {
    int i, j, k;
    for (i = 0; i < R1; i++) {
        for (j = 0; j < C2; j++) {
            result[i][j] = 0;
            for (k = 0; k < R2; k++) {
                result[i][j] += mat1[i][k] * mat2[k][j]; // R2 = 2
            }
        }
    }
}

int main() {
    int matrix1[R1][C1], matrix2[R2][C2], result[R1][C2];

    // Seed the random number generator with current time
    srand(time(NULL));

    // membuat 2 matrix random
    create_matrix(R1, C1, matrix1, min1, max1);
    create_matrix(R2, C2, matrix2, min2, max2);

    // menampilkan matrix 1
    printf("-------- Matrix 4x2 --------- :\n");
    display_matrix(R1, C1, matrix1);
    printf("\n");

    // menampilkan matrix 2
    printf("-------- Matrix 2x5 ---------:\n");
    display_matrix(R2, C2, matrix2);
    printf("\n");

    // Perkalian matrix
    multiply(R1, C1, R2, C2, matrix1, matrix2, result);

    // menampilkan hasil perkalian matrix
    printf("PERKALIAN MATRIX:\n");
    display_matrix(R1, C2, result);
    
    // membuat shared memory
    key_t key = 1234;
    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayresult) [5] = shmat (shmid,NULL,0);

    for (int i=0; i<4 ; i++){
        for (int j = 0 ; j<5 ; j++){
            arrayresult [i][j] = result[i][j];
        }
    }

    shmdt((void *) arrayresult);
return 0;
}
