#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <pthread.h>
#include <wait.h>
#include <fcntl.h>

#define MAX_TREE_HT 50
#define STR_SIZE 1000

int status = 0;
char item_arr[]={'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
            'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
            'W', 'X', 'Y', 'Z'};

int size = sizeof(item_arr);


struct keyHuffman {
    char item;
    char key[20];
};

struct MinHNode {
  char item;
  unsigned freq;
  struct MinHNode *left, *right;
};

struct MinHeap {
  unsigned size;
  unsigned capacity;
  struct MinHNode **array;
};

// Create nodes
struct MinHNode *newNode(char item, unsigned freq) {
  struct MinHNode *temp = (struct MinHNode *)malloc(sizeof(struct MinHNode));

  temp->left = temp->right = NULL;
  temp->item = item;
  temp->freq = freq;

  return temp;
}

// Create min heap
struct MinHeap *createMinH(unsigned capacity) {
  struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

  minHeap->size = 0;

  minHeap->capacity = capacity;

  minHeap->array = (struct MinHNode **)malloc(minHeap->capacity * sizeof(struct MinHNode *));
  return minHeap;
}

// Function to swap
void swapMinHNode(struct MinHNode **a, struct MinHNode **b) {
  struct MinHNode *t = *a;
  *a = *b;
  *b = t;
}

// Heapify
void minHeapify(struct MinHeap *minHeap, int idx) {
  int smallest = idx;
  int left = 2 * idx + 1;
  int right = 2 * idx + 2;

  if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
    smallest = left;

  if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
    smallest = right;

  if (smallest != idx) {
    swapMinHNode(&minHeap->array[smallest], &minHeap->array[idx]);
    minHeapify(minHeap, smallest);
  }
}

// Check if size if 1
int checkSizeOne(struct MinHeap *minHeap) {
  return (minHeap->size == 1);
}

// Extract min
struct MinHNode *extractMin(struct MinHeap *minHeap) {
  struct MinHNode *temp = minHeap->array[0];
  minHeap->array[0] = minHeap->array[minHeap->size - 1];

  --minHeap->size;
  minHeapify(minHeap, 0);

  return temp;
}

// Insertion function
void insertMinHeap(struct MinHeap *minHeap, struct MinHNode *minHeapNode) {
  ++minHeap->size;
  int i = minHeap->size - 1;

  while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
    minHeap->array[i] = minHeap->array[(i - 1) / 2];
    i = (i - 1) / 2;
  }
  minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap) {
  int n = minHeap->size - 1;
  int i;

  for (i = (n - 1) / 2; i >= 0; --i)
    minHeapify(minHeap, i);
}

int isLeaf(struct MinHNode *root) {
  return !(root->left) && !(root->right);
}

struct MinHeap *createAndBuildMinHeap(char item[], int freq[], int size) {
  struct MinHeap *minHeap = createMinH(size);

  for (int i = 0; i < size; ++i)
    minHeap->array[i] = newNode(item[i], freq[i]);

  minHeap->size = size;
  buildMinHeap(minHeap);

  return minHeap;
}

struct MinHNode *buildHuffmanTree(char item[], int freq[], int size) {
  struct MinHNode *left, *right, *top;
  struct MinHeap *minHeap = createAndBuildMinHeap(item, freq, size);

  while (!checkSizeOne(minHeap)) {
    left = extractMin(minHeap);
    right = extractMin(minHeap);

    top = newNode('$', left->freq + right->freq);

    top->left = left;
    top->right = right;

    insertMinHeap(minHeap, top);
  }
  return extractMin(minHeap);
}

void encryptHuffman(struct MinHNode *root, int arr[], int top, struct keyHuffman *key_arr) {
  if (root->left) {
    arr[top] = 0;
    encryptHuffman(root->left, arr, top + 1, key_arr);
  }
  if (root->right) {
    arr[top] = 1;
    encryptHuffman(root->right, arr, top + 1, key_arr);
  }
  if (isLeaf(root)) {
    char str[20];
    int str_index = 0;
    for (int i = 0; i < top; ++i) {
      str_index += snprintf(str + str_index, sizeof(str) - str_index, "%d", arr[i]);
    }

    int key_index;
    for (int i = 0; i < size; ++i) {
        if(key_arr[i].item == root->item) {
            key_index = i;
            break;
        }
    }
    strcpy(key_arr[key_index].key, str);
  }
}

struct MinHNode *HuffmanCodes(char item[], int freq[], int size) {
  struct MinHNode *root = buildHuffmanTree(item, freq, size);

  return root;
}

int main(int argc, char *argv[])
{
    int p1[2], //frekuensi
        p2[2], //text ajk
        p3[2], //tree huff
        p4[2]; //enkripsi text
    
    if(pipe(p1) < 0) {
        fprintf(stderr, "Error when making pipe1");
    }

    if(pipe(p2) < 0) {
        fprintf(stderr, "Error when making pipe2");
    }

    if(pipe(p3) < 0) {
        fprintf(stderr, "Error when making pipe 3");
    }

    if(pipe(p4) < 0) {
        fprintf(stderr, "Error when making pipe 4");
    }

    pid_t child_id;

    child_id = fork();

    if(child_id < 0) {
        fprintf(stderr, "Error when forking");
    }

    if(child_id > 0) {
        close(p1[0]);
        close(p2[0]);
        close(p3[1]);
        close(p4[1]);

        system("wget -q --no-check-certificate 'https://docs.google.com/uc?export=download&id=1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C' -O file.txt");
        
        FILE *filePointer;
        char fileContent[STR_SIZE];
        int freq[size];

        filePointer = fopen("file.txt", "r");

        if(filePointer == NULL) {
            fprintf(stderr, "Error on opening file");
        }

        fgets(fileContent, sizeof(fileContent), filePointer);

        fclose(filePointer);

        // inisiasi array freq
        for(int i = 0; i < size; i++) freq[i] = 0;

        // hitung frekuensi
        for(int i = 0; i < strlen(fileContent); i++) {
            fileContent[i] = toupper(fileContent[i]);
            for(int j = 0; j < size; j++)
                if((int) fileContent[i] == (int) item_arr[j])
                    freq[j]++;
        }

        if(write(p1[1], freq, sizeof(freq)) < 0) {
            fprintf(stderr, "Error on writing to pipe 1");
        }

        if(write(p2[1], fileContent, sizeof(fileContent)) < 0) {
            fprintf(stderr, "Error on writing to pipe 2");
        }

        close(p1[1]);
        close(p2[1]);

        system("rm file.txt");

        wait(NULL);

        struct keyHuffman *key_arr = malloc(sizeof(struct keyHuffman) * size);

        if(read(p3[0], key_arr, sizeof(struct keyHuffman) * size) < 0) {
            fprintf(stderr, "Error on reading pipe 3");
        }

        char encryptedFile[STR_SIZE * 15];
        if(read(p4[0], encryptedFile, sizeof(encryptedFile)) < 0) {
            fprintf(stderr, "Error on reading pipe 4");
        }

        close(p3[0]);
        close(p4[0]);

        // proses dekripsi
        char decryptedFile[STR_SIZE*15] = {0};
        int code_index = 0;
        int str_index = 0;
        char str_temp[20];
        for(int i = 0; i < strlen(encryptedFile); i++) {
            str_temp[code_index++] = encryptedFile[i]; for(int j = 0; j < size; j++) {
                if(strcmp(str_temp, key_arr[j].key) == 0) {
                    decryptedFile[str_index++] = key_arr[j].item;
                    code_index = 0;
                    memset(str_temp, 0, 20);
                    break;
                }
            }
        }

        printf("Kompresi file:\n%s\n\n", encryptedFile);
        printf("Dekompresi file:\n%s\n\n", decryptedFile);

        printf("Perbandingan bit:\n");
        printf("Sebelum dikompresi: %d\n", (int)strlen(decryptedFile)*8);
        printf("Setelah dikompresi: %d\n", (int)strlen(encryptedFile));
    } else {
        close(p1[1]);
        close(p2[1]);
        close(p3[0]);
        close(p4[0]);

        int freq[size];

        if(read(p1[0], freq, sizeof(freq)) < 0) {
            fprintf(stderr, "Error on reading pipe 1");
        }

        char fileContent[STR_SIZE];
        if(read(p2[0], fileContent, sizeof(fileContent)) < 0) {
            fprintf(stderr, "Error on reading pipe 2");
        }

        struct MinHNode *tree = HuffmanCodes(item_arr, freq, size);

        struct keyHuffman *key_arr = (struct keyHuffman*) malloc(sizeof(struct keyHuffman) * size);

        for(int i = 0; i < size; i++) {
            key_arr[i].item = item_arr[i];
        }

        int arr[MAX_TREE_HT], top = 0;
        encryptHuffman(tree, arr, top, key_arr);

        char encryptedFile[STR_SIZE*15] = {0};
        int str_index = 0;

        for(int i = 0; i < strlen(fileContent); i++) {
            for(int j = 0; j < size; j++) {
                if(key_arr[j].item == fileContent[i]) {
                  str_index += snprintf(encryptedFile + str_index, sizeof(encryptedFile) - str_index, "%s", key_arr[j].key);
                }
            }
        }

        if(write(p3[1], key_arr, sizeof(struct keyHuffman) * size) < 0) {
            fprintf(stderr, "Error on writing to pipe 3");
        }

        free(tree);
        
        if(write(p4[1], encryptedFile, sizeof(encryptedFile)) < 0) {
            fprintf(stderr, "Error on writing to pipe 4");
        }

        close(p1[0]);
        close(p2[0]);
        close(p3[1]);
        close(p4[1]);
    }

    return EXIT_SUCCESS;
}
