#include <stdio.h>
#include <stdlib.h>
#include <json-c/json.h>
#include <string.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>

struct mesg_buffer
{
    long mesg_type;
    char mesg_text[100];
    char song[100];
    pid_t id;
} message;



char rot13(char c)
{
    if (isalpha(c))
    {
        if ((c >= 'a' && c <= 'm') || (c >= 'A' && c <= 'M'))
        {
            c += 13;
        }
        else
        {
            c -= 13;
        }
    }
    return c;
}

void dekripsi_rot(char *str)
{
    int len = strlen(str);
    for (int i = 0; i < len; i++)
    {
        str[i] = rot13(str[i]);
    }
}

void decrypt()
{
    struct json_object *json;
    struct json_object *obj;
    struct json_object *method;
    struct json_object *song;
    int i;
    int len;

    FILE *fp;
    fp = fopen("playlist.txt", "a"); 

    if (fp == NULL)
    {
        fp = fopen("playlist.txt", "w"); 
        if (fp == NULL)
        {
            printf("playlist.txt error. could not create file\n");
            return;
        }
    }

    json = json_object_from_file("playlist.json");
    len = json_object_array_length(json);

    for (i = 0; i < len; i++)
    {
        obj = json_object_array_get_idx(json, i);
        method = json_object_object_get(obj, "method");
        song = json_object_object_get(obj, "song");

        // rot13
        if (strcmp(json_object_get_string(method), "rot13") == 0)
        {
            char str[366];
            strcpy(str, json_object_get_string(song));
            dekripsi_rot(str);
            fprintf(fp, "%s\n", str);
            // printf("%d. rot13 decrypted\n", i + 1);
        }

        // base64
        else if (strcmp(json_object_get_string(method), "base64") == 0)
        {
            char str[366];
            char decoded[512];
            strcpy(str, json_object_get_string(song));
            char command[512];
            sprintf(command, "echo '%s' | base64 --decode", str);
            FILE *fo = popen(command, "r");
            if (fo == NULL)
            {
                printf("Error: failed to execute command\n");
                return;
            }
            fgets(decoded, 512, fo);
            pclose(fo);
            fprintf(fp, "%s\n", decoded);
            // printf("%d.base64 decrypted\n", i + 1);
        }

        // hex
        else if (strcmp(json_object_get_string(method), "hex") == 0)
        {
            char str[366];
            char decoded[512];

            strcpy(str, json_object_get_string(song));
            char command[512];
            sprintf(command, "echo '%s' | xxd -r -p", str);
            FILE *fo = popen(command, "r");
            if (fo == NULL)
            {
                printf("Error: failed to execute command\n");
                return;
            }
            fgets(decoded, 512, fo);
            pclose(fo);
            fprintf(fp, "%s\n", decoded);
            // printf("%d. hex decrypted\n", i + 1);
        }
    }
    fclose(fp);
    json_object_put(json);
}


void sortsong()
{
    system("sort -f playlist.txt > temp.txt && mv temp.txt playlist.txt");
}

void list()
{
    system("cat playlist.txt | sort");
}

void playsong(char *song)
{
    char command[366], number[10];
    sprintf(command, "grep -qi '%s' playlist.txt", song);

    int com = system(command);

    if (!com)
    {
        int count;
        sprintf(command, "grep -ci '%s' playlist.txt", song);

        FILE *pipe;
        pipe = popen(command, "r");
        if (pipe == NULL)
        {
            perror("popen");
            exit(1);
        }
        fgets(number, sizeof(number), pipe);

        count = atoi(number);
        pclose(pipe);
        // printf("%d\n", count);

        if (count == 1)
        {
            char str[512];

            printf("User %d Playing ", message.id);
            sprintf(command, "grep -i '%s' playlist.txt", song);

            FILE *pipe;
            pipe = popen(command, "r");
            if (pipe == NULL)
            {
                perror("popen");
                exit(1);
            }

            fgets(str, sizeof(str), pipe);
            pclose(pipe);
            printf("%s", str);
        }
        else
        {
            char hasil[1024] = "";
            char str[512];

            printf("There are %d Songs Containing %s:\n", count, song);
            sprintf(command, "grep -i '%s' playlist.txt", song);

            FILE *pipe;
            pipe = popen(command, "r");
            if (pipe == NULL)
            {
                perror("popen");
                exit(1);
            }
            while (fgets(str, sizeof(str), pipe) != NULL)
            {
                strcat(hasil, str);
            }
            pclose(pipe);
            printf("%s", hasil);
        }
    }
    else
    {
        printf("There is no song containing %s", song);
    }
}
void addsong(char *song)
{
    char command[512];
    sprintf(command, "grep -qi '%s' playlist.txt", song);
    int com = system(command);
    // if exist
    if (!com)
    {
        printf("Song Already on Playlist\n");
    }

    // if not exist
    if (com > 0)
    {
        FILE *fp = fopen("playlist.txt", "a");
        fprintf(fp, "%s\n", song);
        fclose(fp);
        printf("User %d Add %s", message.id, message.song);
    }
}

int main()
{
    key_t key;
    int msgid;
    pid_t users[2];
    int userCount = 0;

    key = ftok("progfile", 65);

    msgid = msgget(key, 0666 | IPC_CREAT);

    sem_t *sem;
    sem = sem_open("/semaphore", O_CREAT, 0644, 2);

    while (1)
    {
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        printf("Action: %s\n", message.mesg_text);

        int found_user = 0;
        for (int i = 0; i < userCount; i++)
        {
            if (users[i] == message.id)
            {
                found_user = 1;
                break;
            }
        }
        if (!found_user && userCount == 2)
        {
            printf("STREAM SYSTEM OVERLOAD: User %d cannot send commands at the moment.\n", message.id);
            continue;
        }
        if (!found_user)
        {
            if (sem_wait(sem) == -1)
            {
                perror("sem_wait");
                exit(1);
            }
            users[userCount++] = message.id;
        }

        if (strcasecmp(message.mesg_text, "DECRYPT") == 0)
        {
            decrypt();
            sortsong();
            printf("deryption success\n");
        }
        else if (strcasecmp(message.mesg_text, "LIST") == 0)
        {
            list();
        }
        else if (strcasecmp(message.mesg_text, "PLAY") == 0)
        {
            playsong(message.song);
        }
        else if (strcasecmp(message.mesg_text, "ADD") == 0)
        {
            addsong(message.song);
            sortsong();
            printf("Song added\n");
        }

        else if (strcasecmp(message.mesg_text, "EXIT") == 0)
        {
            int found_user = 0;
            for (int i = 0; i < userCount; i++)
            {
                if (users[i] == message.id)
                {
                    found_user = 1;

                    for (int j = i; j < userCount - 1; j++)
                    {
                        users[j] = users[j + 1];
                    }
                    userCount--;
                    break;
                }
            }

            if (found_user)
            {
                printf("User %d has quit.\n", message.id);
            }
            else
            {
                printf("User %d is not active.\n", message.id);
            }
        }
        else if (strcasecmp(message.mesg_text, "SHUTDOWN") == 0)
        {
            if (msgctl(msgid, IPC_RMID, NULL) == -1)
            {
                perror("msgctl");
                exit(1);
            }

            if (sem_destroy(sem) == -1)
            {
                perror("sem_destroy");
                exit(1);
            }

            exit(0);
        }
        else
        {
            printf("Unknown Command\n");
        }
        if (!found_user)
        {
            if (sem_post(sem) == -1)
            {
                perror("sem_post");
                exit(1);
            }
        }
    }

    
    sem_close(sem);
    sem_unlink("/semaphore");

    return 0;
}
