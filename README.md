# sisop-praktikum-modul-2-2023-BS-D08
> Kelompok D08

***

## Anggota Kelompok
1. I Gusti Ngurah Ervan Juli Ardana (5025211205)
2. Ghifari Maaliki Syafa Syuhada (5025211158)
3. Elmira Farah Azalia (5025211197)

---
### Soal1
#### - Pertanyaan:
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

a. Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

b. Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

c. Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

d. Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 

e. Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

Catatan:
Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh: 
Huruf A	: 01000001
Huruf a	: 01100001
Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya
Agar lebih mudah, ubah semua huruf kecil ke huruf kapital

#### - Solusi:
Sebelum memulai megerjakan, tentunya kita perlu memahami algoritma Huffman. Algoritma huffman yang kami implementasikan menggunakan 2 struct yaitu `MinHNode` yang merupakan node dari heap dan `MinHeap` yang menampung nodenya dan merepresentasikan heap yang akan kita bentuk.
```
struct MinHNode {
  char item;
  unsigned freq;
  struct MinHNode *left, *right;
};

struct MinHeap {
  unsigned size;
  unsigned capacity;
  struct MinHNode **array;
};
```
Untuk membuat huffman tree pertama-tama kami memanggil wrapper function yang menerima 3 argumen penting, yaitu `item[]` yang mmerupakan list dari karakter yang ingin kita cari frequensinya. Kemudian `freq[]` yang menampung frekuensi dari setiap karakter pada `item[]` dengan indeks yang sama. Terakhir `size` yang merupakan ukuran dari kedua array.
```
struct MinHNode *HuffmanCodes(char item[], int freq[], int size) {
  struct MinHNode *root = buildHuffmanTree(item, freq, size);

  return root;
}
```
Kemudian wrapper function akan memanggil buildHuffmanTree yang akan membuat huffman treenya.
```
struct MinHNode *buildHuffmanTree(char item[], int freq[], int size) {
  struct MinHNode *left, *right, *top;
  struct MinHeap *minHeap = createAndBuildMinHeap(item, freq, size);

  while (!checkSizeOne(minHeap)) {
    left = extractMin(minHeap);
    right = extractMin(minHeap);

    top = newNode('$', left->freq + right->freq);

    top->left = left;
    top->right = right;

    insertMinHeap(minHeap, top);
  }
  return extractMin(minHeap);
}
```
Fungsi diatas pertama akan membuat 3 variabel node bernama left, right, dan top. Lalu fungsi akan memanggil `createAndBuildMinHeap(item, freq, siez)` yang akan membuat min heap berdasarkan item dan frekuensi yang kita berikan ke fungsi sebelumnya. Selanjutnya adalah melakukan looping dimana dalam loop tersebut akan memanggil fungsi `extractMin(minHeap)` yang akan mengambil nilai minimal dari min heap yaitu rootnya. Rootnya kemudian akan dihapus dari heap dan heap tersebut diperbaiki dengan fungsi `minHeapify()`. Dapat dilihat bahwa while loop melakukan `extractMin` sebanyak 2 kali. Node `left` dan `right` ini akan dihubungkan dengan node `top` yang berisikan total frekuensi kedua node. Lalu node `top` akan diinsert kepada min heap. Karena top diinsert lagi maka tentunya suatu saat fungsi `extractMin` akan mengambil node top. Hal ini memang disengaja karena pada huffman tree semua node dianggap memiliki frekuensi dan node yang memiliki frekuensi paling kecil akan berada di paling bawah. Maka hasil akhir dari huffman tree akan mirip dengan max heap. Digunakan min heap karena huffman tree perlu memasangkan kedua node terkecil maka dengan min heap akan jauh lebih efisien karena hanya perlu mengambil root saja dibanding dengan max heap dimana kita perlu mentraverse heapnya sampai kebawah. Karakter pada array `item` yang kita passing kepada fungsi akan berada pada leaf-leafnya. Contoh dari huffman tree adalah sebagai berikut:
![tree](img/1-tree.jpg)

Untuk mentraverse treenya, kami menggunakan fungsi `traverseHuffman` yang akan menulis kodenya sesuai dengan langkah treenya. Apabila melangkah ke kiri maka ditambahkan '0' kepada kodenya sedangkan apabila melagkah ke kanan maka ditambahkan '1' kepada kodenya. Pada contoh diatas, huruf 'f' akan memiliki kode '011'.

Setelah kita tahu bagaimana algoritma huffmannya bekerja, akan kita lanjutkan dengan kode utamanya. Untuk soal ini kami membutuhkan 2 process yang akan berkomunikasi dengan pipe. Kami menginisiasi array alfabet dan size nya pada global agar dapat diakses oleh kedua process. 
```
...
char item_arr[]={'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
            'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
            'W', 'X', 'Y', 'Z'};

int size = sizeof(item_arr);
...
```
Kode huffman dari setiap karakter pada `item_arr` akan disimpan ke dalam struct berikut
```
struct keyHuffman {
    char item;
    char key[20];
};

```
Lalu pada fungsi main kami menggunaan 4 pipe yang dinisiasikan terlebih dahulu kemudian kami melakukan forking.
```
int main(int argc, char *argv[])
{
    int p1[2], //frekuensi
        p2[2], //text ajk
        p3[2], //tree huff
        p4[2]; //enkripsi text
    
    if(pipe(p1) < 0) {
        fprintf(stderr, "Error when making pipe1");
    }

    if(pipe(p2) < 0) {
        fprintf(stderr, "Error when making pipe2");
    }

    if(pipe(p3) < 0) {
        fprintf(stderr, "Error when making pipe 3");
    }

    if(pipe(p4) < 0) {
        fprintf(stderr, "Error when making pipe 4");
    }

    pid_t child_id;

    child_id = fork();

    if(child_id < 0) {
        fprintf(stderr, "Error when forking");
    }
    ...
```
a. Pertama-tama pada parent process kami mendownload file yang diberikan dengan menggunakan perintah `wget` dengan output file.txt. File tersebut kemudian akan dibaca dan isi dari file akan dipindahkan ke variabel `fileContent`. Karakter pada `fileContent` kemudian akan diubah menjadi upper case dengan fungsi `toupper()` dan setiap upper case letter yang muncul akan dihitung frekuensinya pada array `freq`. Lalu `freq` akan di write ke p1 dan `fileContent` akan di write ke p2. Setelah itu file.txt dihapus dan parent process menunggu respon dari child process.

```
    if(child_id > 0) {
        close(p1[0]);
        close(p2[0]);
        close(p3[1]);
        close(p4[1]);

        system("wget -q --no-check-certificate 'https://docs.google.com/uc?export=download&id=1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C' -O file.txt");
        
        FILE *filePointer;
        char fileContent[STR_SIZE];
        int freq[size];

        filePointer = fopen("file.txt", "r");

        if(filePointer == NULL) {
            fprintf(stderr, "Error on opening file");
        }

        fgets(fileContent, sizeof(fileContent), filePointer);

        fclose(filePointer);

        // inisiasi array freq
        for(int i = 0; i < size; i++) freq[i] = 0;

        // hitung frekuensi
        for(int i = 0; i < strlen(fileContent); i++) {
            fileContent[i] = toupper(fileContent[i]);
            for(int j = 0; j < size; j++)
                if((int) fileContent[i] == (int) item_arr[j])
                    freq[j]++;
        }

        if(write(p1[1], freq, sizeof(freq)) < 0) {
            fprintf(stderr, "Error on writing to pipe 1");
        }

        if(write(p2[1], fileContent, sizeof(fileContent)) < 0) {
            fprintf(stderr, "Error on writing to pipe 2");
        }

        close(p1[1]);
        close(p2[1]);

        system("rm file.txt");

        wait(NULL);

    ...
```

b. Pada child process pertama kami membaca p1 dan p2. Kemudian kami membuat huffman treenya dengan memanggil fungsi `HuffmanCodes(item_arr,freq, size)`. Setelah huffman treenya dibuat, kita perlu mentraverse treenya dengan menggunakan fungsi `encryptHuffman(tree, arr, top, key_arr)`, dimana key_arr adalah array dari struct `keyHuffman`. 
```
    } else {
        close(p1[1]);
        close(p2[1]);
        close(p3[0]);
        close(p4[0]);

        int freq[size];

        if(read(p1[0], freq, sizeof(freq)) < 0) {
            fprintf(stderr, "Error on reading pipe 1");
        }

        char fileContent[STR_SIZE];
        if(read(p2[0], fileContent, sizeof(fileContent)) < 0) {
            fprintf(stderr, "Error on reading pipe 2");
        }

        struct MinHNode *tree = HuffmanCodes(item_arr, freq, size);

        struct keyHuffman *key_arr = (struct keyHuffman*) malloc(sizeof(struct keyHuffman) * size);

        for(int i = 0; i < size; i++) {
            key_arr[i].item = item_arr[i];
        }

        int arr[MAX_TREE_HT], top = 0;
        encryptHuffman(tree, arr, top, key_arr);
    ...
```
c. Setelah mendapat pasangan kode huffman dan karakternya, maka hal selanjutnya adalah membaca filenya dan mengubah setiap karakternya menjadi kode huffmannya. Variabel yang memuat `key_arr` dan kompresi algoritma huffman akan di write ke p3 dan p4 dan akan dilanjutkan di parent process.
```
        char encryptedFile[STR_SIZE*15] = {0};
        int str_index = 0;

        for(int i = 0; i < strlen(fileContent); i++) {
            for(int j = 0; j < size; j++) {
                if(key_arr[j].item == fileContent[i]) {
                  str_index += snprintf(encryptedFile + str_index, sizeof(encryptedFile) - str_index, "%s", key_arr[j].key);
                }
            }
        }

        if(write(p3[1], key_arr, sizeof(struct keyHuffman) * size) < 0) {
            fprintf(stderr, "Error on writing to pipe 3");
        }

        free(tree);
        
        if(write(p4[1], encryptedFile, sizeof(encryptedFile)) < 0) {
            fprintf(stderr, "Error on writing to pipe 4");
        }

        close(p1[0]);
        close(p2[0]);
        close(p3[1]);
        close(p4[1]);
    }
```

d. Kemudian parent process akan melakukan write terhadap p3 dan p4, lalu mendekompresi file dari p4 menggunakan pasangan karakter dan kode huffmannya dari p3. Caranya adalah dengan melakukan loop kepada file yang dikompresi, dan mencari kecocokan terhadap setiap kode karakter yang ada. Apabila ditemukan kecocokan maka karakter yang cocok akan ditaruh di string `decryptedFile`
```
    ...
        // proses dekripsi
        char decryptedFile[STR_SIZE*15] = {0};
        int code_index = 0;
        int str_index = 0;
        char str_temp[20];
        for(int i = 0; i < strlen(encryptedFile); i++) {
            str_temp[code_index++] = encryptedFile[i]; for(int j = 0; j < size; j++) {
                if(strcmp(str_temp, key_arr[j].key) == 0) {
                    decryptedFile[str_index++] = key_arr[j].item;
                    code_index = 0;
                    memset(str_temp, 0, 20);
                    break;
                }
            }
        }
    ...
```

e. Terakhir adalah menunjukkan perbandingan bit yang digunakan sebelum dan setelah dikompresi
```
    ...
        printf("Kompresi file:\n%s\n\n", encryptedFile);
        printf("Dekompresi file:\n%s\n\n", decryptedFile);

        printf("Perbandingan bit:\n");
        printf("Sebelum dikompresi: %d\n", (int)strlen(decryptedFile)*8);
        printf("Setelah dikompresi: %d\n", (int)strlen(encryptedFile));
    ...
```

Hasil run:  
![hasil](img/1-run.png)

Frekuensi:
![frek](img/1-frekuensi.png)

Kode huffman:
![kode](img/1-kode.png)
### Soal2
#### - Pertanyaan:
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

a. Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

b. Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)

c. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

maka:

1 2 6 24 120 720 ... ... …
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)

d. Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

#### - Solusi:
a. pada point a, pertama tama kami mendeklarasikan beberapa variabel penting untuk membangun 2 buah matrix diantaranya:
```
int R1 = 4; 
int C1 = 2; 
int R2 = 2; 
int C2 = 5; 
int min1 = 1; 
int max1 = 5;
int min2 = 1;
int max2 = 4;
```
kemudian kami membuat sebuah function yang bernama create_matrix. yang berfungsi untuk membuat matrix dengan ukuran rows x cols. Matrix tersebut akan diisi dengan angka random antara range_min dan range_max. Fungsi ini juga melakukan shuffle terhadap matrix yang telah dibuat.
```
void create_matrix(int rows, int cols, int matrix[rows][cols], int range_min, int range_max) {

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            matrix[i][j] = range_min + (i*cols + j) % (range_max - range_min + 1);
        }
    }

    for (int i = rows*cols - 1; i > 0; i--) {
        int j = rand() % (i + 1);
        int temp = matrix[i / cols][i % cols];
        matrix[i / cols][i % cols] = matrix[j / cols][j % cols];
        matrix[j / cols][j % cols] = temp;
    }
}
```
pada baris pertama fungsi diatas, dilakukan sebuah loop untuk menghasilkan matrix yang terdiri dari bilangan yang random sesuai dengan range min dan range max yang sudah ditentukan. matrix[0][0] diisi dengan range_min. Pada baris kedua, elemen matrix[0][1] diisi dengan range_min + 1. Selanjutnya, elemen pada baris pertama diisi dengan nilai berurutan sesuai dengan penambahan kolom. Ketika kolom mencapai batas cols, elemen pada baris kedua diisi dengan nilai range_min + cols. Begitu seterusnya.

Namun, elemen-elemen tersebut tidak akan acak jika diisi seperti itu. Oleh karena itu, dilakukan pengacakan elemen menggunakan algoritma Fisher-Yates shuffle pada loop. Pertama-tama, loop mengiterasi setiap elemen pada matrix dari elemen terakhir sampai elemen pertama. Dalam setiap iterasi, loop memilih elemen acak dari elemen yang belum diacak dengan cara mengambil nilai j dari rand() % (i + 1), yang akan menghasilkan nilai acak antara 0 dan i. Selanjutnya, nilai dari elemen yang diacak disimpan dalam variabel temp. Kemudian, nilai dari elemen yang diacak diganti dengan nilai dari elemen acak yang dipilih sebelumnya dan sebaliknya Setelah loop selesai, matrix akan berisi elemen-elemen acak antara range_min dan range_max.

Kemudian kita harus membuat sebuah fungsi yang dapat menampilkan matrix tersebut. fungsi tersebut bernama display matrix.
```
void display_matrix(int rows, int cols, int matrix[rows][cols]) {
    int i, j;
    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n"); // menambahkan line ketika column sudah habis
    }
}
```
Selanjutnya kami membuat fungsi multiply yang kami gunakan untuk mengalikan kedua buahh matrix yang telah kami buat sebelumnya menggunakan dua fungsi diatas.
```
void multiply(int R1, int C1, int R2, int C2, int mat1[R1][C1], int mat2[R2][C2], int result[R1][C2]) {
    int i, j, k;
    for (i = 0; i < R1; i++) {
        for (j = 0; j < C2; j++) {
            result[i][j] = 0;
            for (k = 0; k < R2; k++) {
                result[i][j] += mat1[i][k] * mat2[k][j]; // R2 = 2
            }
        }
    }
}
```
Fungsi diatas bekerja dengan melakukan 2 buah looping sebanyak rows matrix 1 dan column matrix 2 kemudian mengalikan matrix1 [i][k] dengan matrix2[k][j] kemudian menjumlahkanya. setelah itu kita membuat main program untuk memanggil semua fungsi tersebut
```
int main() {
    int matrix1[R1][C1], matrix2[R2][C2], result[R1][C2];

    // Seed the random number generator with current time
    srand(time(NULL));

    // membuat 2 matrix random
    create_matrix(R1, C1, matrix1, min1, max1);
    create_matrix(R2, C2, matrix2, min2, max2);

    // menampilkan matrix 1
    printf("-------- Matrix 4x2 --------- :\n");
    display_matrix(R1, C1, matrix1);
    printf("\n");

    // menampilkan matrix 2
    printf("-------- Matrix 2x5 ---------:\n");
    display_matrix(R2, C2, matrix2);
    printf("\n");

    // Perkalian matrix
    multiply(R1, C1, R2, C2, matrix1, matrix2, result);

    // menampilkan hasil perkalian matrix
    printf("PERKALIAN MATRIX:\n");
    display_matrix(R1, C2, result);
```
setelah berhasil memanggil semua fungsi tersebut, kita harus mendefinisikan shared memory pada main program agar prosesnya dapat diakses di program lain.
```
key_t key = 1234;
    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayresult) [5] = shmat (shmid,NULL,0);

    for (int i=0; i<4 ; i++){
        for (int j = 0 ; j<5 ; j++){
            arrayresult [i][j] = result[i][j];
        }
    }

    shmdt((void *) arrayresult);
return 0;
}
```
fungsi diatas pertama tama Pertama, mendeklarasikan variabel key yang bertipe key_t dengan nilai 1234. Kemudian, fungsi shmget digunakan untuk membuat shared memory dengan ukuran 4x5 integer (ukuran matriks hasil perkalian). Selanjutnya, variabel arrayresult dideklarasikan sebagai pointer ke array 2 dimensi dengan 5 kolom, yang menunjuk ke shared memory yang baru dibuat menggunakan fungsi shmat.Setelah itu, dilakukan looping menggunakan dua variabel i dan j untuk menyalin nilai matriks hasil perkalian ke dalam shared memory. Proses ini dilakukan dengan mengakses setiap elemen result[i][j] dan menyalin nilainya ke dalam elemen yang sesuai di arrayresult[i][j].Terakhir, fungsi shmdt dipanggil untuk melepaskan akses ke shared memory setelah selesai digunakan. Fungsi ini menerima satu parameter, yaitu alamat memori dari shared memory yang telah dialokasikan sebelumnya, yang diwakili oleh variabel arrayresult.

Hasil run: 
![Screenshot_from_2023-05-13_14-50-29](/uploads/228f5e8b59e69aef7472a237c835a974/Screenshot_from_2023-05-13_14-50-29.png)

dapat kita lihat dari hasil ketika program dijalankan. program kalian.c akan menghasilkan matrix random yang bersifat inklusif setiap kali program kalian.c dipanggil

b. pada point b kita diminta untuk mengambil variabel perkalian matriks dari program kalian.c. pertama tama kita hanya perlu mendefinisikan kembali shared memory seperti di point sebelumnya dengan key_id yang sama. lalu yang berbeda hanya pada bagian eksekusi di looping rows dan column. pada kode ini kita hanya perlu menampilkan kembali result yang telah kita buat sebelumnya
```
int main() {
    key_t key = 1234;
    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayresult) [5] = shmat (shmid,NULL,0);

    printf ("hasil dari kalian.c\n");
    
    for (int i=0; i<4 ; i++){
        for (int j = 0 ; j<5 ; j++){
            printf("%d ", arrayresult[i][j]);
        }
        printf ("\n");
    }
    printf ("\n");
       
    shmdt((void *) arrayresult);

    return 0;
}
```
Hasil run:
![Screenshot_from_2023-05-13_14-53-54](/uploads/5fc96d8ebfa1803e68e8fed36b084d1c/Screenshot_from_2023-05-13_14-53-54.png)

dapat dilihat dari program diatas bahwa program cinta.c akan mengambil hasil perkalian dari kalian.c menggunakan shared memory

c. pada point c kita diminta untuk membuat sebuah fungsi faktorial pada kode cinta.c. yang harus kita lakukan pertama tama yaitu membuat fungsi factorial itu sendiri
```
void *factorial(void *arg) {
    int(*arrayresult)[5] = (int(*)[5])arg;

    unsigned long long result[4][5];
    
    // Calculate the factorial of each element in the array
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            unsigned long long fact = 1;
            for (int k = 1; k <= arrayresult[i][j]; k++) {
                fact *= k;
            }
            result[i][j] = fact; // Cast the result back to int
        }
    }
    printf("Factorial Array:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%llu ", result[i][j]);
        }        
    }
    printf ("\n");

    pthread_exit(NULL);
}
```
pada fungsi diatas kita mendeklarasikan  array lokal result dengan ukuran 4 baris dan 5 kolom dideklarasikan untuk menyimpan hasil faktorial dari setiap elemen dalam array argumen.kemudian kita melakukan nested loop sebanyak 3 kali dengan tujuan melakukan perkalian faktorial terhadap masing masing matrix result. kemudian hasil dari semua hasil faktorial akan disimpan di variabel result[i][j].setelah itu kita hanya perlu melakukan looping kembali sebanyak rows dan column untuk menampilkan semua hasi faktorial tersebut menggunakan printf. terakhir untuk melakukan exit pada thread tersebut kita harus memanggil pthread_exit.

Setelah berhasil membuat fungsi factorial tersebut kita melakukan multithreading
```
 // factorial
    pthread_t tid [1];
       for (int i = 0; i < 1 ; i++) {
        pthread_create(&tid[i], NULL, &factorial, (void *) arrayresult);
        }

       for (int i = 0; i < 1; i++) {
        pthread_join(tid[i], NULL);
       }
```
Pertama-tama, kita menyiapkan variabel tid dengan tipe pthread_t dan ukuran array 1, yang akan menyimpan thread identifier untuk setiap thread yang akan dibuat. Kemudian, kita menggunakan loop for untuk membuat thread yang akan menjalankan fungsi factorial, sebanyak satu kali. Pada setiap iterasi, kita memanggil fungsi pthread_create(). Setelah selesai membuat semua thread, kita menggunakan loop for untuk menjalankan semua thread dengan memanggil fungsi pthread_join(). Fungsi ini akan mengunci thread pemanggil hingga thread yang ditentukan selesai dieksekusi, sehingga menghindari terjadinya race condition pada pengaksesan shared memory. 

Hasil run:
![Screenshot_from_2023-05-13_14-56-22](/uploads/f580a0534aa2ac4497f3c024a8cdc680/Screenshot_from_2023-05-13_14-56-22.png)

dari gambar diatas program cinta.c juga akan menampilkan hasil faktorial dari setiap perkalian matrix

d. pada point d kita hanya diminta untuk memodifikasi program cinta.c dengan menghilangkan multithreading pada program tersebut. yang harus kita lakukan sebenarnya sangat mirip dengan cara kita sebelumnya, namun hanya berbeda di bagian main program.
```
int main() {
    key_t key = 1234;
    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayresult) [5] = shmat (shmid,NULL,0);

    printf ("hasil dari kalian.c\n");
    
    for (int i=0; i<4 ; i++){
        for (int j = 0 ; j<5 ; j++){
            printf("%d ", arrayresult[i][j]);
        }
        printf ("\n");
    }
    printf ("\n");

    unsigned long long result[4][5];
    factorial(arrayresult, result);

    printf("Factorial Array:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%llu ", result[i][j]);
        }        
    }
    printf ("\n");

    shmdt((void *) arrayresult);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
``` 
pertama tama kita harus menyalin fungsi factorial yang telah kita buat sebelumnya. kemudian kita membuat main program dengan mendefinisikan shared memory terlebih dahulu agar dapat mengakses memory dari program sebelumnya. setelah berhasil mendefinisikan shared memory dengan key id yang sama kemudian kita hanya perlu melakukan looping 2 kali sebanyak rows dan column dan mencetak kembali result matrix dengan cara melakukan printf dan memanggil arrayresult. Kemudian kami Memanggil fungsi factorial(), yang akan menghitung faktorial setiap elemen dalam array arrayresult. Hasil faktorial akan disimpan di dalam array result. dan mencetaknya dengan perintah printf. terakhir kami harus Melepaskan akses ke shared memory, menggunakan fungsi shmdt(). dan Menghapus shared memory yang telah dibuat, menggunakan fungsi shmctl().

Kemudian, mengenai  perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak yaitu kita bisa lihat dari lamanya waktu yang dibutuhkan untuk menjalankan program tersebut:

![Screenshot_from_2023-05-13_14-28-05](/uploads/c562e8fcb8b4e3ebf595a1a81232a7c9/Screenshot_from_2023-05-13_14-28-05.png)
![Screenshot_from_2023-05-13_14-28-14](/uploads/37579af901cd715e337825afbf0afccd/Screenshot_from_2023-05-13_14-28-14.png)

dari dua gambar diatas dapat dilihat program cinta.c (menggunakan thread dan multithreading) lebih cepat dalam memproses perintah. Hal ini dikarenakan multithreading memungkinkan program untuk menjalankan beberapa tugas secara simultan, sehingga dapat meningkatkan efisiensi waktu pemrosesan.

### Soal3
#### - Pertanyaan:
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

a. Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

b. User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.

c. Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt

d. User juga dapat mengirimkan perintah PLAY <SONG> 

e. User juga dapat menambahkan lagu ke dalam playlist

f. Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.

g. Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".


#### - Solusi:

a. pada point a, kita diminta untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem. yang harus kita lakukan hanya perlu membuat kedua program tersebut terlebih dahulu dan mengambil konsepnya bahwa stream.c merupakan sistem receiver dan user.c merupakan pemberi perintah (input).

b. pada program kedua kita diminta untuk membuat untuk mengatur perintah ketika user mengeluarkan perintah "Decrypt". Pertama tama kita harus membuat sebuah fungsi yg mengatur dekripsi kode yang ada pada file json. 
```
void decrypt()
{
    struct json_object *json;
    struct json_object *obj;
    struct json_object *method;
    struct json_object *song;
    int i;
    int len;

    FILE *fp;
    fp = fopen("playlist.txt", "a"); // open file in append mode

    if (fp == NULL)
    {
        fp = fopen("playlist.txt", "w"); // create file if it doesn't exist
        if (fp == NULL)
        {
            printf("playlist.txt error. could not create file\n");
            return;
        }
    }

    json = json_object_from_file("playlist.json");
    len = json_object_array_length(json);

    for (i = 0; i < len; i++)
    {
        obj = json_object_array_get_idx(json, i);
        method = json_object_object_get(obj, "method");
        song = json_object_object_get(obj, "song");

        // rot13
        if (strcmp(json_object_get_string(method), "rot13") == 0)
        {
            char str[366];
            strcpy(str, json_object_get_string(song));
            rot13_decrypt(str);
            fprintf(fp, "%s\n", str);
            // printf("%d. rot13 decrypted\n", i + 1);
        }

        // base64
        else if (strcmp(json_object_get_string(method), "base64") == 0)
        {
            char str[366];
            char decoded[512];
            strcpy(str, json_object_get_string(song));
            char command[512];
            sprintf(command, "echo '%s' | base64 --decode", str);
            FILE *fo = popen(command, "r");
            if (fo == NULL)
            {
                printf("Error: failed to execute command\n");
                return;
            }
            fgets(decoded, 512, fo);
            pclose(fo);
            fprintf(fp, "%s\n", decoded);
            // printf("%d.base64 decrypted\n", i + 1);
        }

        // hex
        else if (strcmp(json_object_get_string(method), "hex") == 0)
        {
            char str[366];
            char decoded[512];
            strcpy(str, json_object_get_string(song));
            char command[512];
            sprintf(command, "echo '%s' | xxd -r -p", str);
            FILE *fo = popen(command, "r");
            if (fo == NULL)
            {
                printf("Error: failed to execute command\n");
                return;
            }
            fgets(decoded, 512, fo);
            pclose(fo);
            fprintf(fp, "%s\n", decoded);
            // printf("%d. hex decrypted\n", i + 1);
        }
    }
    fclose(fp);
    json_object_put(json);
}
```
Fungsi diatas memiliki tugas untuk membaca data playlist dalam format JSON dari file "playlist.json", mengenkripsi setiap lagu di dalamnya dengan tiga metode enkripsi yang berbeda, yaitu ROT13, Base64, dan Hexadecimal, dan menyimpan lagu-lagu yang telah didekripsi ke dalam file "playlist.txt". fungsi diatas berjalan sebagai berikut:
Pertama-tama, fungsi ini membuka file "playlist.txt" dengan mode append, yang artinya jika file tersebut sudah ada, maka data baru akan ditambahkan pada akhir file, sedangkan jika file belum ada, maka file baru akan dibuat. Jika file tidak dapat dibuka atau dibuat, fungsi akan menampilkan pesan kesalahan dan mengembalikan nilai. Fungsi kemudian membaca data playlist dari file "playlist.json" menggunakan fungsi "json_object_from_file" dari pustaka json-c. Data playlist ini berisi daftar lagu beserta metode enkripsi yang digunakan untuk masing-masing lagu.

Selanjutnya, fungsi melakukan perulangan untuk setiap lagu dalam playlist. Dalam setiap iterasi, fungsi mengambil objek JSON untuk lagu tersebut, dan kemudian mengambil nilai "method" dan "song" dari objek tersebut. Jika metode enkripsi adalah ROT13, fungsi akan memanggil fungsi "rot13_decrypt" untuk mendekripsi lagu menggunakan metode ROT13. Hasil dekripsi kemudian disimpan ke dalam variabel "str", dan kemudian ditulis ke dalam file "playlist.txt" menggunakan fungsi "fprintf". Jika metode enkripsi adalah Base64 atau Hexadecimal, fungsi akan mengubah string lagu yang terenkripsi menjadi bentuk asli menggunakan perintah shell. Perintah shell yang digunakan untuk Base64 adalah "echo <string> | base64 --decode", sedangkan untuk Hexadecimal adalah "echo <string> | xxd -r -p". Hasil dekripsi kemudian disimpan ke dalam variabel "decoded", dan kemudian ditulis ke dalam file "playlist.txt" menggunakan fungsi "fprintf".

Setelah semua lagu dalam playlist didekripsi, fungsi menutup file "playlist.txt" dan membebaskan memori yang digunakan untuk menyimpan data playlist dengan memanggil fungsi "json_object_put". kemudian program akan memanggil fungsi tersebut ketika terdapat perintah "decrypt" dari user.

Hasil Run:
![Screenshot_from_2023-05-13_15-35-28](/uploads/c85a4a82dae30bcdde7a4ca29490902e/Screenshot_from_2023-05-13_15-35-28.png)
![Screenshot_from_2023-05-13_15-36-52](/uploads/5b00549fce3f8c76e7adbd9cb9f5b400/Screenshot_from_2023-05-13_15-36-52.png)

c. pada point c, kita diminta untuk mengatur perintah LIST. gampang saja, kita hanya perlu membuat fungsi list yg berisi perintah cat dari playlist.txt
```
void list()
{
    system("cat playlist.txt | sort");
}
```
Hasil run:
![Screenshot_from_2023-05-13_15-38-04](/uploads/ca4da8569bda2b929a40a381f54af843/Screenshot_from_2023-05-13_15-38-04.png)

d. pada point d, kita diminta untuk mengatur perintah PLAY. pertama tama kita perlu membuat fungsi play. 
```
void play(char *song)
{
    char cmd[366], num[10];
    sprintf(cmd, "grep -qi '%s' playlist.txt", song);
    int flag = system(cmd);
    if (!flag)
    {
        int count;
        sprintf(cmd, "grep -ci '%s' playlist.txt", song);
        FILE *pipe;
        pipe = popen(cmd, "r");
        if (pipe == NULL)
        {
            perror("popen");
            exit(1);
        }
        fgets(num, sizeof(num), pipe);
        count = atoi(num);
        pclose(pipe);
        // printf("%d\n", count);

        if (count == 1)
        {
            char str[512];
            printf("User %d Playing ", message.id);
            sprintf(cmd, "grep -i '%s' playlist.txt", song);
            FILE *pipe;
            pipe = popen(cmd, "r");
            if (pipe == NULL)
            {
                perror("popen");
                exit(1);
            }
            fgets(str, sizeof(str), pipe);
            pclose(pipe);
            printf("%s", str);
        }
        else
        {
            char result[1024] = "";
            char str[512];
            printf("There are %d Songs Containing %s:\n", count, song);
            sprintf(cmd, "grep -i '%s' playlist.txt", song);
            FILE *pipe;
            pipe = popen(cmd, "r");
            if (pipe == NULL)
            {
                perror("popen");
                exit(1);
            }
            while (fgets(str, sizeof(str), pipe) != NULL)
            {
                strcat(result, str);
            }
            pclose(pipe);
            printf("%s", result);
        }
    }
    else
    {
        printf("There is no song containing %s", song);
    }
}
```
Fungsi play(char *song) digunakan untuk mencari dan memainkan lagu dari file playlist.txt. Fungsi ini memiliki parameter song yang merupakan string yang berisi nama lagu yang ingin dicari dan dimainkan.

Pertama, fungsi ini menggunakan command grep untuk mencari apakah lagu yang dicari ada dalam file playlist.txt. Jika lagu ditemukan, maka fungsi akan menghitung berapa kali lagu tersebut muncul di dalam file playlist.txt menggunakan command grep -ci. Jika lagu hanya muncul satu kali, maka lagu akan dimainkan dan nama lagu akan dicetak ke layar. Jika lagu muncul lebih dari satu kali, fungsi akan mencetak semua nama lagu yang ditemukan ke layar.Jika lagu yang dicari tidak ditemukan, maka fungsi akan mencetak pesan "There is no song containing [nama lagu]" ke layar.

Hasil run:
![Screenshot_from_2023-05-13_15-40-41](/uploads/865e1e134e5ba9b28ccadc2f8eb0ad9e/Screenshot_from_2023-05-13_15-40-41.png)
![Screenshot_from_2023-05-13_15-42-42](/uploads/1bc0eedc1e7dc3cf8c463c574de1d997/Screenshot_from_2023-05-13_15-42-42.png)

e. pada point e, kita diminta untuk mengatur perintah ADD. pertama tama kita perlu membuat sebuah fungsi addsong
```
void addsong(char *song)
{
    char command[512];
    sprintf(command, "grep -qi '%s' playlist.txt", song);
    int com = system(command);
    // if exist
    if (!com)
    {
        printf("Song Already on Playlist\n");
    }

    // if not exist
    if (com > 0)
    {
        FILE *fp = fopen("playlist.txt", "a");
        fprintf(fp, "%s\n", song);
        fclose(fp);
        printf("User %d Add %s", message.id, message.song);
    }
}

```

Fungsi  di atas adalah untuk menambahkan lagu ke dalam playlist jika lagu tersebut belum ada di dalamnya. Pada awalnya, kode ini mencari lagu dalam file playlist.txt menggunakan perintah grep dengan opsi -qi, yang akan mengembalikan nilai 0 jika lagu sudah ada dalam file. Jika lagu sudah ada dalam playlist, maka program akan mencetak pesan "Song Already on Playlist". Namun, jika lagu belum ada dalam playlist, maka program akan membuka file "playlist.txt" dalam mode "a" (append) dan menambahkan lagu ke dalamnya menggunakan fprintf. Selain itu, program juga mencetak pesan "User (id) Add (song)" sebagai tanda bahwa lagu telah ditambahkan ke dalam playlist.

Hasil RUN:
![Screenshot_from_2023-05-13_15-43-56](/uploads/83377b355200a52d9ea71ec128839749/Screenshot_from_2023-05-13_15-43-56.png)
![Screenshot_from_2023-05-13_15-44-51](/uploads/51bcaf5afbaf7abeb31873e95831b49a/Screenshot_from_2023-05-13_15-44-51.png)

f. pada point f, kita diminta untuk membatasi user sebanyak 2 user yang dapat mengakses. pertama tama kita harus menginisialisasi semaphore:
```
```
Kemudian kita harus mengatur isi semaphore tersebut:
```
while (1)
    {
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        printf("Action: %s\n", message.mesg_text);

        int found_user = 0;
        for (int i = 0; i < userCount; i++)
        {
            if (users[i] == message.id)
            {
                found_user = 1;
                break;
            }
        }
        if (!found_user && userCount == 2)
        {
            printf("STREAM SYSTEM OVERLOAD: User %d cannot send commands at the moment.\n", message.id);
            continue;
        }
        if (!found_user)
        {
            if (sem_wait(sem) == -1)
            {
                perror("sem_wait");
                exit(1);
            }
            users[userCount++] = message.id;
        }
```
Pada function diatas, terdapat loop yang akan berjalan terus menerus (while (1)) untuk menerima pesan yang dikirim ke message queue dengan menggunakan fungsi msgrcv(). Pesan tersebut kemudian dicetak pada layar dengan menggunakan printf().Selanjutnya, terdapat variabel found_user yang diinisialisasi dengan nilai 0. Variabel tersebut akan digunakan untuk mengecek apakah pengguna (user) yang mengirim pesan sudah terdaftar di sistem atau belum. Setelah itu, terdapat loop for yang akan mengecek semua pengguna yang sudah terdaftar pada sistem. Jika pengguna yang mengirim pesan sudah terdaftar pada sistem, variabel found_user akan di-set menjadi 1. Jika tidak, maka variabel found_user akan tetap bernilai 0.

Jika pengguna yang mengirim pesan belum terdaftar dan jumlah pengguna yang terdaftar sudah mencapai 2, maka pesan "STREAM SYSTEM OVERLOAD: User [id pengguna] cannot send commands at the moment." akan dicetak pada layar dan program akan melanjutkan ke loop berikutnya. Kemudian Jika pengguna yang mengirim pesan belum terdaftar dan jumlah pengguna yang terdaftar masih kurang dari 2, maka program akan menambahkan id pengguna ke dalam array users dengan menggunakan sem_wait() untuk melakukan lock pada mutex sehingga penambahan id pengguna dapat dilakukan dengan aman. Setelah itu, variabel userCount akan di-increment dan program akan melanjutkan ke loop berikutnya.

Hasil run :
![Screenshot_from_2023-05-13_15-47-37](/uploads/e7147f52b94ff113687839bccf7568fc/Screenshot_from_2023-05-13_15-47-37.png)

g. pada point g, kita diminta ketika terdapat command yang tidak dikenal maka program akan memberikan pesan "UNKNOWN COMMAND". Kita hanya perlu membuat sebuah kondisi else (ketika diluar semua command yang tealh ditentukan) yang mengirim pesan unknown command
```
else
        {
            printf("Unknown Command\n");
        }
```
Hasil run:

![Screenshot_from_2023-05-13_15-49-42](/uploads/4e09bba4d38403af002bbae0c97e2f2c/Screenshot_from_2023-05-13_15-49-42.png)

### Soal4
#### - Pertanyaan:
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 
a.	Download dan unzip file tersebut dalam kode c bernama unzip.c.

b.	Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya.Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.

c.	Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file

d.	Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

e.	Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
•	Path dimulai dari folder files atau categorized
•	Simpan di dalam log.txt
•	ACCESSED merupakan folder files beserta dalamnya
•	Urutan log tidak harus sama

f.	Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
•	Untuk menghitung banyaknya ACCESSED yang dilakukan.
•	Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
•	Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

#### - Solusi:
a. Pertama, buat dulu unzip.c menggunakan nano unzip.c.
setelah itu buat algoritma pemrogaman dengan tujuan untuk mendownload dan unzip file.
berikut adalah kode unzip.c.
```
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void* DownloadFile(void* arg) {
    const char* url = (const char*)arg;
    char command[256];

    sprintf(command, "wget --no-check-certificate '%s' -O hehe.zip", url);

    system(command);

    pthread_exit(NULL);
}

int main() {
    pthread_t thread;
    const char* url = "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp";
    const char* zipFilename = "hehe.zip";

    if (pthread_create(&thread, NULL, DownloadFile, (void*)url) != 0) {
        fprintf(stderr, "Failed to create thread.\n");
        return 1;
    }

    pthread_join(thread, NULL);

    system("unzip hehe.zip -d hehe");

    return 0;
}
```
-Fungsi DownloadFile untuk menerima argumen berupa URL file yang akan diunduh. Di dalam fungsi ini, string `url` diubah menjadi perintah `wget` yang akan menjalankan proses unduhan file dengan menggunakan perintah shell. Hasil unduhan akan disimpan dalam file "hehe.zip". Setelah unduhan selesai, thread akan diakhiri dengan memanggil `pthread_exit`.

--Fungsi main akan membuat thread baru  menggunakan `pthread_create`. Thread akan menjalankan fungsi `DownloadFile`, dengan argumen berupa URL file yang akan diunduh. Jika pembuatan thread gagal, pesan kesalahan akan dicetak ke `stderr` dan program akan mengembalikan nilai 1.Setelah thread selesai dieksekusi dengan menggunakan `pthread_join`, program akan mengekstrak file "hehe.zip" dengan menggunakan perintah shell `unzip`. Hasil ekstraksi akan disimpan dalam direktori "hehe".

Selanjutnya penjelasan 4b-4e 
Membuat kode melalui nano categorize.c
```

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>

#define MAX_PATH_LENGTH 256
#define MAX_EXTENSIONS 100
#define MAX_THREADS 10

typedef struct {
    char srcPath[MAX_PATH_LENGTH];
    char dstPath[MAX_PATH_LENGTH];
} MoveThreadArgs;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

char extensions[MAX_EXTENSIONS][10];
int numExtensions = 0;
int maxCounts[MAX_EXTENSIONS] = {0};
int otherCount = 0;
```
Pada subbab ini, terdapat beberapa header file yang di-include, seperti stdio.h, stdlib.h, string.h, dan lain-lain. Header file tersebut menyediakan fungsi-fungsi standar yang digunakan dalam program.
Terdapat pula definisi beberapa konstanta seperti MAX_PATH_LENGTH, MAX_EXTENSIONS, dan MAX_THREADS. Konstanta-konstanta ini digunakan untuk mengatur ukuran maksimum dari beberapa elemen dalam program.

```
void lowercase(char* str) {
    while (*str) {
        *str = tolower((unsigned char)*str);
        str++;
    }
}

void getTime(char* timeStr) {
    time_t rawtime;
    struct tm* timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(timeStr, 20, "%d-%m-%Y %H:%M:%S", timeinfo);
}

void ensureFolderExists(const char* folderPath) {
    struct stat st;
    if (stat(folderPath, &st) == -1) {
        // Folder belum ada, buat folder baru
        if (mkdir(folderPath, 0777) != 0) {
            printf("Error creating folder: %s\n", folderPath);
            exit(1);
        }
    }
}

void moveFileToCategory(const char* fileName, const char* category) {
    char sourcePath[MAX_PATH_LENGTH];
    char destPath[MAX_PATH_LENGTH];
    sprintf(sourcePath, "hehe/files/%s", fileName);
    sprintf(destPath, "hehe/files/categorized/%s/%s", category, fileName);

    // Memindahkan file
    if (rename(sourcePath, destPath) != 0) {
        printf("Error moving file: %s\n", fileName);
        exit(1);
    }
}

void getFolderPath(const char* filePath, const char* rootFolder, char* folderPath) {
    char* relativePath = strstr(filePath, rootFolder);
    if (relativePath != NULL) {
        strncpy(folderPath, filePath, relativePath - filePath + strlen(rootFolder));
        folderPath[relativePath - filePath + strlen(rootFolder)] = '\0';
    } else {
        strcpy(folderPath, "");
    }
}

void ensureLogFileExists() {
    FILE* logFile = fopen("hehe/log.txt", "a");
    if (!logFile) {
        // File log.txt belum ada, buat file baru
        logFile = fopen("hehe/log.txt", "w");
        if (!logFile) {
            printf("Error creating log file\n");
            return;
        }
        fclose(logFile);
    }
}

void ensureCategorizedFolderExists() {
    char folderPath[MAX_PATH_LENGTH];
    snprintf(folderPath, MAX_PATH_LENGTH, "hehe/files/categorized");

    ensureFolderExists(folderPath);
}

void ensureFoldersExist() {
    ensureFolderExists("hehe");
    ensureFolderExists("hehe/files");
    ensureCategorizedFolderExists();
}

void logAccessed(const char* folderPath) {
	ensureLogFileExists();
    char timeStr[20];
    getTime(timeStr);
    FILE* logFile = fopen("hehe/log.txt", "a");
    if (logFile) {
    fprintf(logFile,"%s ACCESSED [%s]\n", timeStr, folderPath);
    fclose(logFile);
}
}

void logMoved(const char* extension, const char* srcPath, const char* dstPath) {
    ensureLogFileExists();
	char timeStr[20];
    getTime(timeStr);
    FILE* logFile = fopen("hehe/log.txt", "a");
    if (logFile) {
        fprintf(logFile, "%s MOVED [%s] file: [%s] > [%s]\n", timeStr, extension, srcPath, dstPath);
        fclose(logFile);
    }
}

void logMade(const char* folderName) {
    ensureLogFileExists();
	char timeStr[20];
    getTime(timeStr);
    FILE* logFile = fopen("hehe/log.txt", "a");
    if (logFile) {
        fprintf(logFile, "%s MADE [%s]\n", timeStr, folderName);
        fclose(logFile);
    }
}

void* moveFile(void* arg) {
    MoveThreadArgs* threadArgs = (MoveThreadArgs*)arg;
    char srcPath[MAX_PATH_LENGTH];
    char dstPath[MAX_PATH_LENGTH];

    strcpy(srcPath, threadArgs->srcPath);
    strcpy(dstPath, threadArgs->dstPath);

    free(arg);

    pthread_mutex_lock(&mutex);
    logMoved(srcPath, srcPath, dstPath);
    pthread_mutex_unlock(&mutex);

    if (rename(srcPath, dstPath) != 0) {
        pthread_mutex_lock(&mutex);
        printf("Error moving file: %s\n", srcPath);
        pthread_mutex_unlock(&mutex);
    }

    pthread_cond_signal(&cond);
    return NULL;
}

void handleFile(const char* filePath, const char* rootFolder) {
    char extension[10];
    char dstFolder[MAX_PATH_LENGTH];
    char dstPath[MAX_PATH_LENGTH];

    int i;
    for (i = strlen(filePath) - 1; i >= 0; i--) {
        if (filePath[i] == '.') {
            strcpy(extension, &filePath[i + 1]);
            lowercase(extension);
            break;
        }
    }

if (i < 0) {
    strcpy(extension, "other");
}

pthread_mutex_lock(&mutex);

for (i = 0; i < numExtensions; i++) {
    if (strcmp(extension, extensions[i]) == 0) {
        break;
    }
}

if (i == numExtensions) {
        if (numExtensions >= MAX_EXTENSIONS) {
            pthread_mutex_unlock(&mutex);
            pthread_mutex_lock(&mutex);
            printf("Maximum number of extensions exceeded\n");
            pthread_mutex_unlock(&mutex);
            return;
        }

        if (strcmp(extension, "other") == 0) {
            pthread_mutex_lock(&mutex);
            otherCount++;
            pthread_mutex_unlock(&mutex);
        } else {
            strcpy(extensions[numExtensions], extension);
            numExtensions++;
            logMade(extension);
        }
    }
    
int count = maxCounts[i]++;
pthread_mutex_unlock(&mutex);

char folderPath[MAX_PATH_LENGTH];
getFolderPath(filePath, rootFolder, folderPath);

if (i == numExtensions - 1 && count >= maxCounts[i]) {
    snprintf(dstFolder, MAX_PATH_LENGTH + 44, "hehe/files/%s/%s (%d)", folderPath, extension, count + 2);
} else {
    snprintf(dstFolder, MAX_PATH_LENGTH + 44, "hehe/files/%s/%s", folderPath, extension);
}
snprintf(dstPath, MAX_PATH_LENGTH + 44, "hehe/files/%s/%s", dstFolder, filePath);

pthread_mutex_lock(&mutex);

DIR* dir = opendir("hehe/files");
if (!dir) {
    if (mkdir(dstFolder, 0777) != 0) {
        pthread_mutex_unlock(&mutex);
        pthread_mutex_lock(&mutex);
        printf("Error creating folder: %s\n", dstFolder);
        pthread_mutex_unlock(&mutex);
        return;
    }
    logMade(dstFolder);
} else {
    closedir(dir);
}

pthread_mutex_unlock(&mutex);

MoveThreadArgs* threadArgs = (MoveThreadArgs*)malloc(sizeof(MoveThreadArgs));
snprintf(threadArgs->srcPath, MAX_PATH_LENGTH, "hehe/files/categorized/%s", filePath);
snprintf(threadArgs->dstPath, MAX_PATH_LENGTH + 44, "hehe/files/categorized/%s", dstPath);

pthread_mutex_lock(&mutex);
logMoved(extension, filePath, dstPath);
pthread_mutex_unlock(&mutex);

pthread_t tid;
pthread_create(&tid, NULL, moveFile, threadArgs);

pthread_mutex_lock(&mutex);
while (pthread_cond_wait(&cond, &mutex) != 0);
pthread_mutex_unlock(&mutex);
}

void* processFiles(void* arg) {
char folderPath[MAX_PATH_LENGTH];
strcpy(folderPath, (char*)arg);
DIR* dir = opendir(folderPath);
if (!dir) {
printf("Error opening folder: %s\n", folderPath);
return NULL;
}
struct dirent* entry;
while ((entry = readdir(dir)) != NULL) {
    if (entry->d_type == DT_REG) {
        handleFile(entry->d_name, folderPath);
    } else if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
        char subFolderPath[MAX_PATH_LENGTH];
        snprintf(subFolderPath, MAX_PATH_LENGTH + 256, "%s/%s", folderPath, entry->d_name);
        logAccessed(subFolderPath);

        pthread_t tid;
        pthread_create(&tid, NULL, processFiles, strdup(subFolderPath));
    }
}

closedir(dir);
free(arg);
return NULL;
}

void printFileCounts() {
    printf("File counts by extension (ascending order):\n");

    // Buat salinan array maxCounts dan extensions
    int counts[MAX_EXTENSIONS];
    char extensionsCopy[MAX_EXTENSIONS][10];
    memcpy(counts, maxCounts, sizeof(maxCounts));
    memcpy(extensionsCopy, extensions, sizeof(extensions));

    // Bubble sort untuk mengurutkan counts dan extensionsCopy
    for (int i = 0; i < numExtensions - 1; i++) {
        for (int j = 0; j < numExtensions - i - 1; j++) {
            if (strcmp(extensionsCopy[j], extensionsCopy[j + 1]) > 0) {
                int tempCount = counts[j];
                counts[j] = counts[j + 1];
                counts[j + 1] = tempCount;

                char tempExtension[10];
                strcpy(tempExtension, extensionsCopy[j]);
                strcpy(extensionsCopy[j], extensionsCopy[j + 1]);
                strcpy(extensionsCopy[j + 1], tempExtension);
            }
        }
    }

    // Cetak counts dan extensionsCopy
    for (int i = 0; i < numExtensions; i++) {
        printf("%s : %d\n", extensionsCopy[i], counts[i]);
    }

    printf("other : %d\n", otherCount);
    printf("\n");
}
```
Pada subbab ini, terdapat beberapa fungsi bantuan yang akan digunakan dalam program.

Fungsi lowercase digunakan untuk mengubah semua karakter dalam sebuah string menjadi huruf kecil.

Fungsi getTime digunakan untuk mendapatkan waktu saat ini dalam format tertentu.

Fungsi ensureFolderExists digunakan untuk memastikan bahwa sebuah folder ada. Jika folder belum ada, fungsi ini 
akan membuat folder baru.

Fungsi moveFileToCategory digunakan untuk memindahkan sebuah file ke dalam kategori tertentu berdasarkan ekstensi file tersebut.

Fungsi getFolderPath digunakan untuk mendapatkan path folder dari sebuah file.

Fungsi ensureLogFileExists digunakan untuk memastikan bahwa file log.txt ada. Jika file belum ada, fungsi ini akan membuat file baru.

Fungsi ensureCategorizedFolderExists digunakan untuk memastikan bahwa folder "categorized" ada di dalam folder "files".

Fungsi ensureFoldersExist digunakan untuk memastikan bahwa seluruh folder yang diperlukan dalam program ada.

Fungsi logAccessed digunakan untuk mencatat akses ke sebuah folder pada file log.txt.

Fungsi logMoved digunakan untuk mencatat pemindahan file dari satu lokasi ke lokasi lain pada file log.txt.

Fungsi logMade digunakan untuk mencatat pembuatan folder pada file log.txt.

Fungsi moveFile merupakan fungsi yang akan dijalankan oleh thread untuk memindahkan file dari satu lokasi ke lokasi lain.

Fungsi handleFile digunakan untuk menangani file dengan menentukan ekstensi file, memindahkan file ke kategori yang sesuai, dan mencatat log.

Fungsi processFiles merupakan fungsi yang akan dijalankan oleh thread untuk memproses file dalam sebuah folder.

Fungsi printFileCounts digunakan untuk mencetak jumlah file per ekstensi yang telah dihitung.
```
int main() {
FILE* extFile = fopen("hehe/extensions.txt", "r");
if (!extFile) {
printf("Error opening extensions.txt\n");
return 1;
}

int main(); {
	ensureFoldersExist();
}

char extension[10];
while (fscanf(extFile, "%s", extension) == 1) {
    lowercase(extension);
    strcpy(extensions[numExtensions], extension);
    numExtensions++;
}

fclose(extFile);
FILE* maxFile = fopen("hehe/max.txt", "r");
if (!maxFile) {
    printf("Error opening max.txt\n");
    return 1;
}

int count;
int i = 0;
while (fscanf(maxFile, "%d", &count) == 1) {
    maxCounts[i] = count;
    i++;
}

fclose(maxFile);

char rootFolder[MAX_PATH_LENGTH] = "hehe/files";
logAccessed(rootFolder);

pthread_t tid;
pthread_create(&tid, NULL, processFiles, strdup(rootFolder));

pthread_join(tid, NULL);
printFileCounts();
printf("\nFile counts by extension:\n");

pthread_mutex_lock(&mutex);
for (int i = 0; i < numExtensions; i++) {
    printf("%s : %d\n", extensions[i], maxCounts[i]);
}
printf("other : %d\n", otherCount);
pthread_mutex_unlock(&mutex);
pthread_mutex_destroy(&mutex);
pthread_cond_destroy(&cond);

return 0;
}
```
Subbab ini merupakan implementasi dari fungsi main() dalam program tersebut. Berikut adalah penjelasan mengenai langkah-langkah yang dilakukan dalam subbab ini:

Membuka file "hehe/extensions.txt" dengan mode "r" menggunakan fungsi fopen. Jika pembukaan file gagal (nilai pointer extFile adalah NULL), maka pesan error akan dicetak ke layar dengan menggunakan printf. Selanjutnya, program akan mengembalikan nilai 1 sebagai indikasi kesalahan.

Memanggil fungsi ensureFoldersExist() untuk memastikan bahwa semua folder yang diperlukan telah ada.

Membaca setiap string dari file extensions.txt menggunakan fscanf. Setiap string yang berhasil dibaca akan diubah menjadi huruf kecil menggunakan fungsi lowercase dan disalin ke array extensions. Jumlah ekstensi yang berhasil dibaca akan dihitung menggunakan variabel numExtensions.

Menutup file extensions.txt dengan menggunakan fclose.

Membuka file "hehe/max.txt" dengan mode "r" menggunakan fungsi fopen. Jika pembukaan file gagal, pesan error akan dicetak dan program akan mengembalikan nilai 1.

Membaca setiap integer dari file max.txt menggunakan fscanf. Setiap integer yang berhasil dibaca akan disimpan ke dalam array maxCounts. Variabel i digunakan untuk melacak indeks saat membaca setiap angka.

Menutup file max.txt dengan menggunakan fclose.

Mendefinisikan variabel rootFolder yang merupakan path folder utama yang akan diproses oleh program. Nilai awalnya diatur sebagai "hehe/files".

Mencatat akses ke folder utama dengan memanggil fungsi logAccessed dan menyediakan rootFolder sebagai argumen.

Membuat thread dengan menggunakan pthread_create untuk menjalankan fungsi processFiles dengan argumen rootFolder. Nilai pengembalian dari fungsi ini disimpan dalam variabel tid.

Menunggu thread selesai menggunakan pthread_join.

Mencetak jumlah file per ekstensi yang telah dihitung dengan memanggil fungsi printFileCounts.

Mencetak daftar jumlah file per ekstensi yang diizinkan (maxCounts), termasuk jumlah file "other", dengan menggunakan mutex untuk sinkronisasi akses ke variabel bersama.

Menghancurkan mutex dan kondisi dengan menggunakan pthread_mutex_destroy dan pthread_cond_destroy.

Program selesai dan mengembalikan nilai 0 sebagai indikasi sukses.


Untuk 4f yang dilakukan pertama membuat kode logchecker pada nano logchecker.C
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <ctype.h>
#include <time.h>

typedef struct{
    char direktori[1000];
    int total;
} direk;

direk allFolder[1000];
direk extension[100];

int cmpfunc(const void *a, const void *b) {
    const direk *rec1 = (direk *)a;
    const direk *rec2 = (direk *)b;
    return strcmp(rec1->direktori,rec2->direktori);
}
int main(int argc, char const *argv[])
{
    FILE *f;
    f = fopen("hehe/log.txt","r");

    if (f == NULL) {
        printf("Failed to open file\n");
        return 1;
    }

    char s[1000];
    int count = 0;
    int idx = 0;
    int idx2 = 0;

    while (fgets(s, sizeof(s), f)) {
        s[strcspn(s, "\n")] = '\0';
        if(strstr(s, "ACCESSED") != NULL){
            count++;
        }
        
        if(strstr(s, "MADE") != NULL){
            char *k = strrchr(s, '/');
            if (k != NULL) {
                allFolder[idx].total = 0;
                strcpy(allFolder[idx++].direktori, k+1);
                char *l= strchr(k,'(');
                if (l == NULL)
                {
                    extension[idx2].total = 0;
                    strcpy(extension[idx2++].direktori, k+1);
                }
                
            }
        }
        if(strstr(s, "MOVED") != NULL){
            char *temp = strrchr(s, '/');
            if (temp != NULL) {
                for (int i = 0; i < idx; i++) {
                    if (strcmp(allFolder[i].direktori, temp+1) == 0) {
                        allFolder[i].total++;
                    }
                }
            }
        }
    }

    fclose(f);
    printf("ACCESSED: %d",count);
    qsort(allFolder,idx,sizeof(direk),cmpfunc);
    for (int i = 0; i < idx; i++) {
        printf("%s : %d\n", allFolder[i].direktori, allFolder[i].total);
    }
    for (int i = 0; i < idx2; i++){
        for (int j = 0; j < idx; j++){
            if (strstr(allFolder[j].direktori,extension[i].direktori)!=NULL){
                extension[i].total+=allFolder[j].total;
            }
        }
    }
    printf("Folder Ekstensi\n");
    qsort(extension,idx2,sizeof(direk),cmpfunc);
    for (int i = 0; i < idx2; i++) {
        printf("%s : %d\n", extension[i].direktori, extension[i].total);
    }
    return 0;
}
```
Program ini membaca file log dan menghitung jumlah akses, jumlah file dalam setiap direktori, dan jumlah file dalam setiap direktori ekstensi yang tercatat dalam log.txt tersebut.



#### - Kendala
Masih belum bisa moving file dan membaca ekstensi dengan benar

Mengakibatkan log.checker tidak menghitung ACCESSED dengan benar

#### - Revisi
Program log.checker harus menghasilkan output jumlah accessed dan nama file ekstensi

Program categorize membuat dua fungsi a dan b, dimana fungsi b logika mengakses suatu file, lalu fungsi a membuat thread untuk fungsi b
