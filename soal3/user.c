#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>

struct mesg_buffer
{
    long mesg_type;
    char mesg_text[100];
    char song[100];
    pid_t id;
} message;

int main()
{
    key_t key;
    int msgid;
    key = ftok("progfile", 65);
    msgid = msgget(key, 0666 | IPC_CREAT);
    message.mesg_type = 1;

    while (1)
    {
        printf("Input Command : ");
        scanf("%s", message.mesg_text);
        strcpy(message.song, "");

        pid_t user_pid = getpid();
        message.id = user_pid;

        if (strcasecmp(message.mesg_text, "PLAY") == 0)
        {
            char temp;
            scanf("%c", &temp);
            scanf("%[^\n]", message.song);
        }
        else if (strcasecmp(message.mesg_text, "ADD") == 0)
        {
            char temp;
            scanf("%c", &temp);
            scanf("%[^\n]", message.song);
        }
        
        msgsnd(msgid, &message, sizeof(message), 0);
        printf("command : %s %s\n", message.mesg_text, message.song);
    
        if (strcasecmp(message.mesg_text, "EXIT") == 0)
        {
            return 0;
        }
    }

    return 0;
}